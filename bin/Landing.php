<?php
/**
 * Created by PhpStorm.
 * User: alcatraz1907
 * Date: 13.07.2016
 * Time: 18:16
 */
require('../vendor/autoload.php');

class Landing extends PHPUnit_Framework_TestCase {

	protected $url = 'http://comegetsomegarcinia.com/';
	/**
	 * @var \RemoteWebDriver
	 */
	protected $webDriver;

	public function setUp()
	{
		$capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'firefox');
		$this->webDriver = RemoteWebDriver::create('http://localhost:4444/wd/hub', $capabilities);
	}



//	public function testClickButton()
//	{
//		$this->webDriver->get($this->url);
//
//		// find search field by its id
//		$search = $this->webDriver->findElement(WebDriverBy::className('btnYellow'));
//		$search->click();
//	}

	public function testSendForm(){

		$this->webDriver->get('http://landing.comegetsomegarcinia.com/');

		$select_country = new WebDriverSelect($this->webDriver->findElement(WebDriverBy::name('personal[country]')));
		$select_country->selectByValue('UA');
		$this->webDriver->findElement(WebDriverBy::name('personal[firstname]'))->sendKeys('sssss');
		$this->webDriver->findElement(WebDriverBy::name('personal[lastname]'))->sendKeys('sssss');
		$this->webDriver->findElement(WebDriverBy::name('personal[email]'))->sendKeys('sssss@mail.ru');
		$this->webDriver->findElement(WebDriverBy::name('personal[phone]'))->sendKeys('561651652222');
		$this->webDriver->findElement(WebDriverBy::name('personal[address]'))->sendKeys('sssss');
		$this->webDriver->findElement(WebDriverBy::name('personal[city]'))->sendKeys('sssss');

		$select_state = new WebDriverSelect($this->webDriver->findElement(WebDriverBy::name('personal[state]')));
		$select_state ->selectByValue('CV');
		$this->webDriver->findElement(WebDriverBy::name('personal[zip]'))->sendKeys('456156165');

		$send_data = $this->webDriver->findElement(
		// some CSS selectors can be very long:
			WebDriverBy::cssSelector("button[type='submit']")
		);

		$send_data->click();
		$action = $this->webDriver->findElement(WebDriverBy::id('order-form'))->getAttribute('action');
		$this->assertEquals(
			'https://secure.nutritionsupplementsorder.com/',
			$action
		);

		$this->webDriver->get('https://secure.nutritionsupplementsorder.com/');

		$select_country = new WebDriverSelect($this->webDriver->findElement(WebDriverBy::name('personal[country]')));
		$select_country->selectByValue('UA');

		$this->assertNotEmpty($this->webDriver->findElement(WebDriverBy::name('personal[firstname]'))->getAttribute('value'));
		$this->assertNotEmpty($this->webDriver->findElement(WebDriverBy::name('personal[lastname]'))->getAttribute('value'));
		$this->assertNotEmpty($this->webDriver->findElement(WebDriverBy::name('personal[email]'))->getAttribute('value'));
		$this->assertNotEmpty($this->webDriver->findElement(WebDriverBy::name('personal[phone]'))->getAttribute('value'));
		$this->assertNotEmpty($this->webDriver->findElement(WebDriverBy::name('personal[address]'))->getAttribute('value'));
		$this->assertNotEmpty($this->webDriver->findElement(WebDriverBy::name('personal[city]'))->getAttribute('value'));
		$this->assertNotEmpty($this->webDriver->findElement(WebDriverBy::name('personal[state]'))->getAttribute('value'));
		$this->webDriver->findElement(WebDriverBy::name('card[number]'))->sendKeys('1234567890123456');
		$this->webDriver->findElement(WebDriverBy::name('card[cvv]'))->sendKeys('123');

		$select_state = new WebDriverSelect($this->webDriver->findElement(WebDriverBy::name('personal[statecode]')));
		$select_state ->selectByValue('CV');

		$this->assertNotEmpty($this->webDriver->findElement(WebDriverBy::name('personal[zip]'))->getAttribute('value'));

		$firstResult = $this->webDriver->findElement(
		// some CSS selectors can be very long:
			WebDriverBy::cssSelector("button[type='submit']")
		);

		$firstResult->click();

	}

//	public function testOrderSendForm(){
//		$this->webDriver->get('https://secure.nutritionsupplementsorder.com/');
//
//		$select_country = new WebDriverSelect($this->webDriver->findElement(WebDriverBy::name('personal[country]')));
//		$select_country->selectByValue('UA');
//		$this->webDriver->findElement(WebDriverBy::name('personal[firstname]'))->sendKeys('sssss');
//		$this->webDriver->findElement(WebDriverBy::name('personal[lastname]'))->sendKeys('sssss');
//		$this->webDriver->findElement(WebDriverBy::name('personal[email]'))->sendKeys('sssss@mail.ru');
//		$this->webDriver->findElement(WebDriverBy::name('personal[phone]'))->sendKeys('561651652222');
//		$this->webDriver->findElement(WebDriverBy::name('personal[address]'))->sendKeys('sssss');
//		$this->webDriver->findElement(WebDriverBy::name('personal[city]'))->sendKeys('sssss');
//		$this->webDriver->findElement(WebDriverBy::name('personal[state]'))->sendKeys('CV');
//
//		$select_state = new WebDriverSelect($this->webDriver->findElement(WebDriverBy::id('personal[statecode]')));
//		$select_state ->selectByValue('CV');
//		$this->webDriver->findElement(WebDriverBy::name('personal[zip]'))->sendKeys('456156165');
//
//		$firstResult = $this->webDriver->findElement(
//		// some CSS selectors can be very long:
//			WebDriverBy::cssSelector("button[type='submit']")
//		);
//
//		$firstResult->click();
//
//		$this->assertEquals(
//			'https://secure.nutritionsupplementsorder.com/',
//			$this->webDriver->findElement(WebDriverBy::id('order-form'))->getAttribute('action')
//		);
//	}

	protected function waitForUserInput()
	{
		if(trim(fgets(fopen("php://stdin","r"))) != chr(13)) return;
	}

	public function tearDown()
	{
		$this->webDriver->close();
	}
}
 