<?php
/**
 * Created by PhpStorm.
 * User: alcatraz1907
 * Date: 13.07.2016
 * Time: 17:37
 */
require('../vendor/autoload.php');

class PreLanding extends PHPUnit_Framework_TestCase {

	protected $url = 'http://comegetsomegarcinia.com/';
	/**
	 * @var \RemoteWebDriver
	 */
	protected $webDriver;

	public function setUp()
	{
		$capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'firefox');
		$this->webDriver = RemoteWebDriver::create('http://localhost:4444/wd/hub', $capabilities);
	}

	public function tearDown()
	{
		$this->webDriver->close();
	}

	public function testSearch()
	{
		$this->webDriver->get($this->url);

		// find search field by its id
		$search = $this->webDriver->findElement(WebDriverBy::className('btnYellow'));
		$search->click();
	}


	protected function waitForUserInput()
	{
		if(trim(fgets(fopen("php://stdin","r"))) != chr(13)) return;
	}

}