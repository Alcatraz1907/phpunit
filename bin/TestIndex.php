<?php
/**
 * Created by PhpStorm.
 * User: alcatraz1907
 * Date: 12.07.2016
 * Time: 12:00
 */
require_once('../vendor/autoload.php');
class TestIndex extends PHPUnit_Framework_TestCase{


	/**
	 * @var \RemoteWebDriver
	 */
	protected $webDriver;

	public function setUp()
	{
		$capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'firefox');
		$this->webDriver = RemoteWebDriver::create('http://localhost:5555/wd/hub', $capabilities);
	}

	protected $url = 'http://phpunittest.loc/';


	public function testGitHubHome()
	{
		$this->webDriver->get($this->url);
		// checking that page title contains word 'GitHub'
		$this->assertContains('GitHub', $this->webDriver->getTitle());
	}


//	public function testHasLoginForm()
//	{
//
//		$this->webDriver->get($this->url);
//		$username = $this->webDriver->tagName('username');
//		$password = $this->webDriver->tagName('password');
//
//		$this->assertEquals('', $username->value());
//		$this->assertEquals('', $password->value());
//	}
} 