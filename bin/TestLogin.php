<?php
/**
 * Created by PhpStorm.
 * User: alcatraz1907
 * Date: 11.07.2016
 * Time: 15:12
 */
require_once '../vendor/autoload.php';

class TestLogin extends PHPUnit_Extensions_Selenium2TestCase{
	protected function setUp()
	{
		$this->setHost('0.0.0.0');
		$this->setPort(5555);
		$this->setBrowser("firefox");
		$this->setBrowserUrl("http://phpunittest.loc/");
	}

	public function testHasLoginForm()
	{
		$this->url('http://phpunittest.loc/');

		$username = $this->byName('username');
		$password = $this->byName('password');

		$this->assertEquals('', $username->value());
		$this->assertEquals('', $password->value());
	}


} 