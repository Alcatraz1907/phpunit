<?php
/**
 * Created by PhpStorm.
 * User: alcatraz1907
 * Date: 11.07.2016
 * Time: 16:44
 */
require_once '../vendor/autoload.php';
class WebTest extends PHPUnit_Extensions_Selenium2TestCase
{
	protected function setUp()
	{
		$this->setBrowser('firefox');
		$this->setBrowserUrl('http://www.example.com/');
	}

	public function testTitle()
	{
		$this->url('http://www.example.com/');
		$this->assertEquals('Example WWW Page', $this->title());
	}

}